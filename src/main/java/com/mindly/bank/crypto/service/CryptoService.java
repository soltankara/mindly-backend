package com.mindly.bank.crypto.service;

import com.mindly.bank.common.exception.CryptoNotFoundException;
import com.mindly.bank.common.exception.PortfolioItemNotFoundException;
import com.mindly.bank.common.validator.CryptoValidator;
import com.mindly.bank.crypto.domain.entity.Crypto;
import com.mindly.bank.crypto.domain.entity.Item;
import com.mindly.bank.crypto.domain.entity.ItemStatus;
import com.mindly.bank.crypto.domain.repo.CryptoRepo;
import com.mindly.bank.crypto.domain.repo.ItemRepo;
import com.mindly.bank.crypto.dto.CryptoDTO;
import com.mindly.bank.crypto.dto.CryptoPriceDTO;
import com.mindly.bank.crypto.dto.ItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Service
public class CryptoService {

    CryptoRepo cryptoRepo;
    ItemRepo itemRepo;

    @Autowired
    public CryptoService(CryptoRepo cryptoRepo, ItemRepo itemRepo) {
        this.cryptoRepo = cryptoRepo;
        this.itemRepo = itemRepo;
    }

    public Item savePortfolioItem(ItemDTO itemDTO) throws CryptoNotFoundException {
        Crypto c = new Crypto(itemDTO.getCrypto().getId(), itemDTO.getCrypto().getName(), itemDTO.getCrypto().getShortName());
        Item i = new Item(c, itemDTO.getAmount(), itemDTO.getWalletLoc(), itemDTO.getDate(), itemDTO.getLastPrice());
        CryptoValidator.cryptoValidator(c);
        i = itemRepo.save(i);
        return i;
    }

    public Item deletePortfolioItem(Long id) throws PortfolioItemNotFoundException {
        Item i = itemRepo.findById(id).orElse(null);
        CryptoValidator.itemValidator(i);
        try {
            i = Item.deleteItem(i);
            itemRepo.save(i);
        } catch (Exception e) {
            e.getMessage();
        }
        return i;
    }

    public List<Item> getAllPortfolioItems() {

        List<Item> items = itemRepo.getActivePortfolioItems();
        RestTemplate restTemplate = new RestTemplate();

        for (Item item: items) {
            String c = item.getCrypto().getShortName()+"eur";
            System.out.println(c);
            ResponseEntity<CryptoPriceDTO> responseEntity =
                    restTemplate.getForEntity("https://api.bitfinex.com/v1/pubticker/"+c,
                            CryptoPriceDTO.class);

            BigDecimal lastPrice = new BigDecimal(responseEntity.getBody().getLast_price());
            lastPrice = lastPrice.multiply(item.getAmount()).setScale(0, RoundingMode.UP);
            item.setLastPrice(String.valueOf(lastPrice));
        }

        return items;
    }

    public List<Crypto> getAllCryptos() {
        return cryptoRepo.findAll();
    }

    public Crypto saveCrypto(Crypto crypto) {
        Crypto c = new Crypto(crypto.getId(), crypto.getName(), crypto.getShortName());
        c = cryptoRepo.save(c);
        return c;
    }

    public Crypto deleteCrypto(Long id) throws CryptoNotFoundException {
        Crypto c = cryptoRepo.findById(id).orElse(null);
        CryptoValidator.cryptoValidator(c);
        try {
            cryptoRepo.delete(c);
        } catch (Exception e) {
            e.getMessage();
        }
        return c;
    }

    public Boolean deletePortfolioItems() {
        try {
            itemRepo.deleteAll();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
