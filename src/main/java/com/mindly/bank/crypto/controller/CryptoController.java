package com.mindly.bank.crypto.controller;

import com.mindly.bank.common.exception.CryptoNotFoundException;
import com.mindly.bank.common.exception.PortfolioItemNotFoundException;
import com.mindly.bank.crypto.domain.entity.Crypto;
import com.mindly.bank.crypto.domain.entity.Item;
import com.mindly.bank.crypto.dto.CryptoDTO;
import com.mindly.bank.crypto.dto.ItemDTO;
import com.mindly.bank.crypto.service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController(value = "/")
@CrossOrigin
public class CryptoController {

    @Autowired
    CryptoService cryptoService;


    @PostMapping(value = "item")
    public ResponseEntity<ItemDTO> savePortfolioItem(@RequestBody ItemDTO itemDTO) throws CryptoNotFoundException {
        ItemDTO item = new ItemDTO(cryptoService.savePortfolioItem(itemDTO));
        return new ResponseEntity<>(item, new HttpHeaders(), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "item/{id}")
    public ResponseEntity<ItemDTO> deletePortfolioItem(@PathVariable Long id) throws PortfolioItemNotFoundException {
        ItemDTO item = new ItemDTO(cryptoService.deletePortfolioItem(id));
        return new ResponseEntity<>(item, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping(value = "items/")
    public ResponseEntity<Boolean> deletePortfolioItems() {
        Boolean result = cryptoService.deletePortfolioItems();
        return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "items")
    public ResponseEntity<List<ItemDTO>> getAllPortfolioItems() {
        List<Item> items = cryptoService.getAllPortfolioItems();
        List<ItemDTO> itemDTOS = new ArrayList<>();

        for (Item item : items) {
            itemDTOS.add(new ItemDTO(item));
        }

        return new ResponseEntity<>(itemDTOS, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "cryptos")
    public ResponseEntity<List<CryptoDTO>> getAllCryptos(){
        List<Crypto> cryptos = cryptoService.getAllCryptos();
        List<CryptoDTO> cryptoDTOS = new ArrayList<>();

        for (Crypto crypto : cryptos) {
            cryptoDTOS.add(new CryptoDTO(crypto));
        }

        return new ResponseEntity<>(cryptoDTOS, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping(value = "crypto")
    public ResponseEntity<CryptoDTO> saveCrypto(@RequestBody Crypto crypto) {
        CryptoDTO c =  new CryptoDTO(cryptoService.saveCrypto(crypto));
        return new ResponseEntity<>(c, new HttpHeaders(), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "crypto/{id}")
    public ResponseEntity<CryptoDTO> deleteCrypto(@PathVariable Long id) throws PortfolioItemNotFoundException, CryptoNotFoundException {
        CryptoDTO crypto = new CryptoDTO(cryptoService.deleteCrypto(id));
        return new ResponseEntity<>(crypto, new HttpHeaders(), HttpStatus.OK);
    }

}
