package com.mindly.bank.crypto.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CryptoPriceDTO {
    String mid;
    String bid;
    String ask;
    String last_price;
    String low;
    String high;
    String volume;
    String timestamp;
}
