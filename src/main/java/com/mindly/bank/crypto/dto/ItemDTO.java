package com.mindly.bank.crypto.dto;

import com.mindly.bank.crypto.domain.entity.Crypto;
import com.mindly.bank.crypto.domain.entity.Item;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class ItemDTO{

    private Long id;
    private CryptoDTO crypto;
    private BigDecimal amount;
    private String walletLoc;
    private LocalDate date;
    private String lastPrice;


    public ItemDTO(Item item) {
        this.id = item.getId();
        this.amount = item.getAmount();
        this.walletLoc = item.getWalletLoc();
        this.date = item.getDate();
        this.crypto = new CryptoDTO(item.getCrypto());
        this.lastPrice = item.getLastPrice();
    }

}
