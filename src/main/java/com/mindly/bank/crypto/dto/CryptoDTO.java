package com.mindly.bank.crypto.dto;

import com.mindly.bank.crypto.domain.entity.Crypto;
import lombok.*;
import org.springframework.hateoas.ResourceSupport;

@Data
public class CryptoDTO{

    Long id;
    String name;
    String shortName;

    public CryptoDTO (Crypto crypto) {
        this.id = crypto.getId();
        this.name = crypto.getName();
        this.shortName = crypto.getShortName();
    }
}
