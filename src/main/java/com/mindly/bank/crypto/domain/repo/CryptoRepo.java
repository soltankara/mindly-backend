package com.mindly.bank.crypto.domain.repo;

import com.mindly.bank.crypto.domain.entity.Crypto;
import com.mindly.bank.crypto.domain.repoCustom.CustomCryptoRepo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoRepo extends JpaRepository<Crypto, Long>, CustomCryptoRepo {
}
