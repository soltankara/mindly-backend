package com.mindly.bank.crypto.domain.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Crypto {

    @Id
    @GeneratedValue
    Long id;

    String name;
    String shortName;
}
