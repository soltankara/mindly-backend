package com.mindly.bank.crypto.domain.repo;

import com.mindly.bank.crypto.domain.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepo extends JpaRepository<Item, Long> {

    @Query("SELECT items from Item items where items.status = 'ACTIVE'")
    List<Item> getActivePortfolioItems();
}
