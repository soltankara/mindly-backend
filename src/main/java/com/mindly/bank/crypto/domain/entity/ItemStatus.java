package com.mindly.bank.crypto.domain.entity;

public enum ItemStatus {
    ACTIVE, INACTIVE
}
