package com.mindly.bank.crypto.domain.entity;

import com.mindly.bank.crypto.dto.ItemDTO;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class Item {

    @Id
    @GeneratedValue
    Long id;

    @OneToOne
    Crypto crypto;
    BigDecimal amount;
    String walletLoc;
    LocalDate date;
    String lastPrice;

    @Enumerated(EnumType.STRING)
    ItemStatus status;

    public Item(Crypto crypto, BigDecimal amount, String walletLoc, LocalDate date, String lastPrice) {
        this.crypto = crypto;
        this.amount = amount;
        this.walletLoc = walletLoc;
        this.date = date;
        this.lastPrice = lastPrice;
        this.status = ItemStatus.ACTIVE;
    }

    public static Item deleteItem(Item item) {
        item.status = ItemStatus.INACTIVE;
        return item;
    }
}
