package com.mindly.bank.common.exception;

public class CryptoNotFoundException extends Exception {
    public CryptoNotFoundException() {
        super(CryptoNotFoundException.class.getName() + " : " + "Crypto Currency not found!");
    }
}
