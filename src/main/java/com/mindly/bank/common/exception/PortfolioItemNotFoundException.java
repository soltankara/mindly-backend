package com.mindly.bank.common.exception;

public class PortfolioItemNotFoundException extends Exception {
    public PortfolioItemNotFoundException() {
        super(PortfolioItemNotFoundException.class.getName() + " : " + "Portfolio Item not found!");
    }
}