package com.mindly.bank.common.validator;

import com.mindly.bank.common.exception.CryptoNotFoundException;
import com.mindly.bank.common.exception.PortfolioItemNotFoundException;
import com.mindly.bank.crypto.domain.entity.Crypto;
import com.mindly.bank.crypto.domain.entity.Item;
import org.springframework.validation.DataBinder;

public class CryptoValidator {

//    private static DataBinder dataBinder;

    public static void cryptoValidator(Crypto crypto) throws CryptoNotFoundException{
        if (crypto == null)
            throw new CryptoNotFoundException();
    }

    public static void itemValidator(Item item) throws PortfolioItemNotFoundException{
        if (item == null)
            throw new PortfolioItemNotFoundException();
    }

}
